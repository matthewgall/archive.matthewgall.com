---
title: "Cozmo... how does a little robot get to me?"
date: 2018-03-24T13:33:32+01:00
image: "images/posts/cozmo.jpg"
---

It's no secret that I'm a bit of a nerd, and when I came across a little emotionally intelligent robot that was available for purchase, I knew I had to get one and try it out.

## A little about Cozmo
[Cozmo](https://www.anki.com/en-gb/cozmo) is billed as an emotionally intelligent robot by its creators [Anki](https://www.anki.com), an American robotics and artificial intelligence company. Capable of recognizing faces and animals via a powerful on-board camera, Cozmo is able to understand his world and surroundings.

## Taking over the office
One of the more amazing revelations was Cozmo's ability to shift people's perspective of him from the third person, a thing with no emotional ability, to being addressed in the first person. A device, being recognized as acutely human.

Even bringing members of the [Cloudflare](https://www.cloudflare.com) office "intense happiness" when they were recognized by him:

{{< instagram Bb4EiDJDC2h >}}